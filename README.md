## 工具

### BitBucket

采用 BitBucket 作为 项目代码 Git 托管站点, 当项目立项时, 在 BitBucket 对应的 Organizations 下创建项目代码中央仓库。

### SourceTree

采用 SourceTree 将简化 Git 操作。 只要你喜欢, 你也可以选择通过命令行操作。

## 工作流程

1. 开发者在本地仓库中新建一个专门的分支开发功能/BugFix。

2. 开发者 push 分支修改到项目 BitBucket 仓库中。

3. 开发者通过 BitBucket 发起一个 Pull Request。

4. 团队的其它成员 Review code，讨论并修改。

5. 项目维护者合并功能到官方仓库中并关闭 Pull Request。

6. 项目的相应分支自动部署上线。

### 分支

中央仓库上有 2 个只读分支 develop, master。

develop 分支是可测试的代码, 将会自动部署到测试环境。

master 分支是在测试环境测试没有问题的分支, 将自动/手动部署到生产环境。

### 开始开发

从 中央仓库 clone 代码到本机, 并在开发环境下运行起来。

### 新功能开发

基于 develop 分支创建名为 feature/<name> 的功能分支, 其中 name 可以是功能的名字, 或是来自 JIRA 上的 issue 编号。
name 可以包含 字母、数字、中划线, 不得使用中文。

### Bug Fix

如进行生产环境的 Bug 修复, 基于 master 分支创建 hotfix/<name> 的修复分支, 其中 name 是来自 JIRA 上的 issue 编号。

### Pull Request

当功能开发完毕或者 Bug 修复完成后, push 代码到中央仓库, 同时创建一个 Pull Request。

创建 Pull Request 时, 需要核对 原分支 和 目标分支。

如果功能分支, 则推送到 develop 分支。

如果是 bug fix 则还需要创建推送到 master 分支的 Pull Request。


![](assets/gitflow-workflow-pull-request.png)
 

### 分支的删除

当创建 Pull Request 时, 需要保证源分支是可删除的, 当 Reviewers 处理 Pull Request 时, 可能会进行源分支的删除操作。